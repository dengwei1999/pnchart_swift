//
//  BarChartViewController.swift
//  pnchart
//
//  Created by dengwei on 16/1/19.
//  Copyright © 2016年 dengwei. All rights reserved.
//

import UIKit

class BarChartViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "BarChart"
        let barChart = PNBarChart(frame:CGRectMake(0,100,320,200))
        barChart.xLabels = ["X1","X2","X3","X4","X5"]
        barChart.xLabelSkip = 10
        barChart.yValueMax = 100
       // barChart.xLabelWidth = 50
        barChart.yValues = ["10","30","50","40","60"]
        barChart.strokeChart()
        self.view.addSubview(barChart)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
