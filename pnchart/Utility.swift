//
//  Utility.swift
//  pnchart
//
//  Created by dengwei on 16/1/19.
//  Copyright © 2016年 dengwei. All rights reserved.
//


import UIKit

class Utility: NSObject {
    
    class func GetViewController<T>(controllerName:String)->T {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())
        let toViewController = mainStoryboard.instantiateViewControllerWithIdentifier(controllerName) as! T
        return toViewController
    }
    
    class func formatDate(date:NSDate)->String {
        
        let fmt = NSDateFormatter()
        
        fmt.dateFormat = "yyyy-MM-dd"
        let dateString = fmt.stringFromDate(date)
        return dateString
    }
    
    class func showMessage(msg:String) {
        
        let alert = UIAlertView(title: "提醒", message: msg, delegate: nil, cancelButtonTitle: "确定")
        alert.show()
        
    }
    
    class func datetoString()->String{
        // 方式1：用已有日期格式进行转换
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateStyle = NSDateFormatterStyle.MediumStyle
        dateFormatter1.timeStyle = NSDateFormatterStyle.MediumStyle
        let  now = NSDate()
        // 方式2：自定义日期格式进行转换
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.dateFormat = "yyyyMMddHHmmss"
        // Date 转 String
        let nowString = dateFormatter2.stringFromDate(now)
        return nowString
    }
}