//
//  LineChartViewController.swift
//  pnchart
//
//  Created by dengwei on 16/1/19.
//  Copyright © 2016年 dengwei. All rights reserved.
//

import UIKit

class LineChartViewController: UIViewController,PNChartDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "LineChart"
        let LineChart = PNLineChart(frame:CGRectMake(0,100,320,200))
        
        LineChart.setXLabels(["X1","X2","X3","X4","X5"], withWidth: 50)
        
        let dataArray = [5,3,1,2,7]
        let LineData = PNLineChartData()
        LineData.color = UIColor.redColor()
        LineData.itemCount = (UInt)(dataArray.count)
        LineData.getData = ({(index:UInt)->PNLineChartDataItem in
            let y:CGFloat = (CGFloat)(dataArray[(Int)(index)])
            return PNLineChartDataItem(y:y)
        })
        
        
        let dataArray2 = [1,4,5,8,2]
        let LineData2 = PNLineChartData()
        LineData2.color = UIColor.blueColor()
        LineData2.itemCount = (UInt)(dataArray2.count)
        LineData2.inflexionPointStyle = .Circle
        LineData2.getData = ({(index:UInt)->PNLineChartDataItem in
            let y:CGFloat = (CGFloat)(dataArray2[(Int)(index)])
            return PNLineChartDataItem(y:y)
        })
        
        LineChart.chartData = [LineData,LineData2]
        LineChart.strokeChart()
        self.view.addSubview(LineChart)

        LineData.dataTitle = "dengwei"
        LineData2.dataTitle = "dengke"
        LineChart.legendStyle = PNLegendItemStyle.Stacked
        let legend = LineChart.getLegendWithMaxWidth(320)
        legend.frame = CGRectMake(0, 300, 320, 100)
        self.view.addSubview(legend)
        
        
        //更新数据
       /*legend.removeFromSuperview()
        
        let dataArray3 = [2,5,9,1,4]
        let LineData3 = PNLineChartData()
        LineData3.color = UIColor.yellowColor()
        LineData3.itemCount = (UInt)(dataArray3.count)
        LineData3.getData = ({(index:UInt)->PNLineChartDataItem in
            let y:CGFloat = (CGFloat)(dataArray3[(Int)(index)])
            return PNLineChartDataItem(y:y)
        })
        
        LineData3.dataTitle = "superx"
        LineChart.updateChartData([LineData3,LineData])
        LineChart.strokeChart()
        
        let legend2 = LineChart.getLegendWithMaxWidth(320)
        legend2.frame = CGRectMake(0, 300, 320, 100)
        self.view.addSubview(legend2)*/
        //
        
        LineChart.delegate = self
        
        
        // Do any additional setup after loading the view.
    }

    func userClickedOnLinePoint(point: CGPoint, lineIndex: Int) {
        print("lineIndex=\(lineIndex),point=\(point)")
    }
    
    func userClickedOnLineKeyPoint(point: CGPoint, lineIndex: Int, pointIndex: Int) {
        print("lineIndex=\(lineIndex),point=\(point),pointIndex=\(pointIndex)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
