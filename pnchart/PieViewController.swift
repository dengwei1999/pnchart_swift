//
//  PieViewController.swift
//  pnchart
//
//  Created by dengwei on 16/1/19.
//  Copyright © 2016年 dengwei. All rights reserved.
//

import UIKit

class PieViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "PieChart"
        
        var items = [PNPieChartDataItem(value: 30, color: UIColor.redColor(),description: "dengwei"),
        PNPieChartDataItem(value: 30, color: UIColor.blueColor(),description: "nieqian"),
        PNPieChartDataItem(value: 40, color: UIColor.yellowColor(),description: "dengke")]
        let pieChart = PNPieChart(frame: CGRectMake(20, 200, 320, 200),items: items)
        
        pieChart.descriptionTextColor = UIColor.whiteColor()
        pieChart.descriptionTextFont = UIFont.systemFontOfSize(14)
        pieChart.strokeChart()
        self.view.addSubview(pieChart)
        
        let legend = pieChart.getLegendWithMaxWidth(320)
        legend.frame = CGRectMake(0, 400, 320,20)
        self.view.addSubview(legend)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
