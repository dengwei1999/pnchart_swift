//
//  CircleViewController.swift
//  pnchart
//
//  Created by dengwei on 16/1/19.
//  Copyright © 2016年 dengwei. All rights reserved.
//

import UIKit

class CircleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "circleChart"
        
        //let CircleChart = PNCircleChart(frame: CGRectMake(0, 100, 320, 200),total: NSNumber(double: 100), current:NSNumber(double:60),clockwise:false)
        let CircleChart = PNCircleChart(frame: CGRectMake(0, 100, 320, 200), total: NSNumber(double: 100), current: NSNumber(double:60), clockwise: false, shadow: true, shadowColor: UIColor.redColor())
        CircleChart.strokeColor = UIColor.blueColor()
        CircleChart.strokeChart()
        self.view.addSubview(CircleChart)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
