//
//  ViewController.swift
//  pnchart
//
//  Created by dengwei on 16/1/19.
//  Copyright © 2016年 dengwei. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button1: UIButton!
    
    @IBOutlet weak var button2: UIButton!
    
    @IBOutlet weak var button3: UIButton!
    
    @IBOutlet weak var button4: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.button1.addTarget(self, action: "payType:", forControlEvents: UIControlEvents.TouchUpInside)
        self.button2.addTarget(self, action: "payType:", forControlEvents: UIControlEvents.TouchUpInside)
        self.button3.addTarget(self, action: "payType:", forControlEvents: UIControlEvents.TouchUpInside)
         self.button4.addTarget(self, action: "payType:", forControlEvents: UIControlEvents.TouchUpInside)
        // Do any additional setup after loading the view, typically from a nib.
    }

    func payType(sender:UIButton){
        switch sender.tag{
        case 1:
            lineChart()
            return
        case 2:
           barChart()
            return
        case 3:
           circleChart()
            return
        case 4:
            pieChart()
            return
        default:
            lineChart()
            return
        }
        
    }
    
    func lineChart(){
        print("linechat")
        let toViewController:LineChartViewController =  Utility.GetViewController("LineChartView")
        self.navigationController?.pushViewController(toViewController, animated: true)
    }
    
    func barChart(){
        print("barchat")
        let toViewController:BarChartViewController =  Utility.GetViewController("BarChartView")
        self.navigationController?.pushViewController(toViewController, animated: true)
    }
    
    func circleChart(){
        print("circleChart")
        let toViewController:CircleViewController =  Utility.GetViewController("CircleView")
        self.navigationController?.pushViewController(toViewController, animated: true)
    }
    
    func pieChart(){
        print("pieChart")
        let toViewController:PieViewController =  Utility.GetViewController("PieView")
        self.navigationController?.pushViewController(toViewController, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

